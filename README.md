# boobytrap hook for Arch Linux

## Installation
```
sudo cp -r c/mkinitcpio-boobytrap/etc /
```

## Configuration

Edit `/etc/mkinitcpio.conf` and insert the boobytrap hook right before the encrypt hook like this:

```
HOOKS=(base udev autodetect modconf block keyboard boobytrap encrypt filesystems fsck)
```

After any change you make to the config, you need to regenerate the initramfs to apply them:

```
sudo mkinitcpio -P
```

If you use secure boot, you will need to sign the images too.

### Disable for fallback image
It may make sense, not to use the boobytrap in the fallback image.
This makes it possible to boot, even if the boobytarp hook should fail.

Add the boobytrap hook to the fallback options in `/etc/mkinitcpio.d/linux.preset`:

```
fallback_options="-S autodetect,boobytrap"
```
