#!/usr/bin/bash

if [[ $EUID -ne 0 ]]; then
  echo "This script needs root! Restarting with sudo"
  exec sudo "$0" "$@"
fi

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

copy_file() {
    echo "Installing $1"
    mkdir -p "$(dirname $1)"
    cp "${SCRIPT_DIR}$1" "$1"
}

copy_file /etc/boobytrap/boobytrap.conf
copy_file /etc/initcpio/hooks/boobytrap
copy_file /etc/initcpio/install/boobytrap
echo

mkinitcpio -P
echo

if command -v sbupdate; then
  echo Signing efi binaries
  sbupdate
fi
